package fr.kapsule.charactr.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "cards")
public class Card implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String symbol;
    private String signification;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Card setSymbol(String symbol) {
        this.symbol = symbol;
        return this;
    }

    public Card setSignification(String signification) {
        this.signification = signification;
        return this;
    }

    public Card setPersonalNotes(String personalNotes) {
        this.personalNotes = personalNotes;
        return this;
    }

    private String personalNotes;

    public Card(String symbol, String signification, String personalNotes){
        this.symbol = symbol;
        this.signification = signification;
        this.personalNotes = personalNotes;
    }

    @Ignore
    public Card(int id, String symbol, String signification, String personalNotes){
        this.id = id;
        this.symbol = symbol;
        this.signification = signification;
        this.personalNotes = personalNotes;
    }



    public String getSymbol() {
        return symbol;
    }

    public String getSignification() {
        return signification;
    }

    public String getPersonalNotes() {
        return personalNotes;
    }
}
