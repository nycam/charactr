package fr.kapsule.charactr.models;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface CardDAO {
    @Insert
    void insert(Card c);

    @Query("Select * from cards")
    List<Card> getAllCards();

    @Delete
    void remove(Card card);

    @Query("select * from cards where id = :id")
    Card selectFromId(int id);

    @Update
    void update(Card card);
}
