package fr.kapsule.charactr.models;

import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import java.util.Observer;

@android.arch.persistence.room.Database(entities = {Card.class}, version = 1)

public abstract class Database extends RoomDatabase {
    private static Database INSTANCE = null;

    //private CardMOCK card_fake = new CardMOCK();

    /*public CardDAO cards(){
        return card_fake;
    }*/



    public abstract CardDAO cards();

    public static Database getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), Database.class, "database")
                    .allowMainThreadQueries()
                    .build();
        }

        return INSTANCE;
    }
}
