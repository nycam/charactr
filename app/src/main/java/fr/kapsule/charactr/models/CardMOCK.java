package fr.kapsule.charactr.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


/**
 * Utilisé pour avoir un jeu de fausse données, ne doit-être utilisé que pour tester
 */
public class CardMOCK implements CardDAO {

    private int id = 0;

    public int getNextId() {
        id++;
        return id;
    }

    List<Card> list_of_cards = new ArrayList<Card>();

    public CardMOCK() {
        list_of_cards.addAll(Arrays.asList(
                new Card(getNextId(), "我", "Je", ""),
                new Card(getNextId(),"你", "Tu", ""),
                new Card(getNextId(),"他", "Il", ""),
                new Card(getNextId(),"好", "Bien", ""),
                new Card(getNextId(),"家", "Maison, Famille", ""),
                new Card(getNextId(),"叫", "S'appeler", ""),
                new Card(getNextId(),"再", "Encore", ""),
                new Card(getNextId(),"看", "voir, regarder", ""),
                new Card(getNextId(),"国", "Pays", ""),
                new Card(getNextId(),"茶", "Thé", ""),
                new Card(getNextId(),"大", "Grand", ""),
                new Card(getNextId(),"小", "Petit", ""),
                new Card(getNextId(),"汉语", "Langue chinoises (mandarin)", ""),
                new Card(getNextId(),"说", "parler", ""),
                new Card(getNextId(),"是", "etre", ""),
                new Card(getNextId(),"吃", "Manger", "")

                ));
    }

    @Override
    public void insert(Card c) {
        c.setId(getNextId());
        list_of_cards.add(c);
        System.out.println("id is" + c.getId());
    }

    @Override
    public List<Card> getAllCards() {
        return new ArrayList<Card>(list_of_cards);
    }

    @Override
    public void remove(Card _card) {
        list_of_cards.removeIf(card -> card.getId() == _card.getId());
    }

    @Override
    public Card selectFromId(int id) {
        Iterator<Card> i = list_of_cards.iterator();
        while(i.hasNext()) {
            Card c = i.next();
            if (c.getId() == id) {
                return c;
            }
        }
        return null;
    }

    @Override
    public void update(Card card) {
        for (int i = 0; i < list_of_cards.size(); i++) {
            if(list_of_cards.get(i).getId() == card.getId()) {
                list_of_cards.set(i, card);
                return;
            }
        }
    }
}
