package fr.kapsule.charactr.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

import eu.davidea.flipview.FlipView;
import fr.kapsule.charactr.R;
import fr.kapsule.charactr.models.Card;

public class TinderCard extends RecyclerView.Adapter<TinderCard.CardView> {

    private List<Card> list_of_cards;

    public TinderCard(List<Card> cards) {
        list_of_cards = cards;
    }

    @NonNull
    @Override
    public CardView onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View card = inflater.inflate(R.layout.flippable_card, viewGroup, false);
        return new CardView(card);
    }

    @Override
    public void onBindViewHolder(@NonNull CardView cardView, int i) {
        Card c = list_of_cards.get(i);
        ((TextView)cardView.view.getFrontLayout().findViewById(R.id.currentCardSymbol)).setText(c.getSymbol());
        ((TextView)cardView.view.getRearLayout().findViewById(R.id.backcard_meaning)).setText(c.getSignification());
    }

    @Override
    public int getItemCount() {
        return list_of_cards.size();
    }

    class CardView extends RecyclerView.ViewHolder {

        public FlipView view;

        private boolean flipped = false;

        public CardView(View card) {
            super(card);
            view = card.findViewById(R.id.flippablecard);
        }
    }

}
