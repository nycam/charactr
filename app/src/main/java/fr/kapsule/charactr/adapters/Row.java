package fr.kapsule.charactr.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import fr.kapsule.charactr.R;
import fr.kapsule.charactr.activities.AddCard;
import fr.kapsule.charactr.activities.UpdateCard;
import fr.kapsule.charactr.models.Card;
import fr.kapsule.charactr.models.Database;

public class Row extends RecyclerView.Adapter<Row.RowView>{

    private List<Card> list_of_cards;

    public Row(List<Card> cards) {
        list_of_cards = cards;
    }

    public void removeCard(int pos) {
        list_of_cards.remove(pos);
        notifyItemRemoved(pos);
    }

    public void updateCard(int pos, Card c) {
        list_of_cards.set(pos, c);
    }

    @NonNull
    @Override
    public RowView onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View cardView = inflater.inflate(R.layout.symbol_row, viewGroup, false);

        RowView row_View_symbol = new RowView(cardView, context);
        return row_View_symbol;
    }

    @Override
    public void onBindViewHolder(@NonNull RowView rowView, int i) {
        Card current_card = list_of_cards.get(i);
        rowView.meaning.setText(current_card.getSignification());
        rowView.symbol.setText(current_card.getSymbol());
        rowView.card = current_card;
    }

    @Override
    public int getItemCount() {
        return list_of_cards.size();
    }

    public class RowView extends RecyclerView.ViewHolder {

        public TextView meaning;
        public TextView symbol;
        public Button delete;
        public Card card;
        public Context context;

        public static final int EDIT_RESULT = 1;

        public RowView(View row, Context context) {
            super(row);
            this.context = context;
            symbol = row.findViewById(R.id.char_row_symbol);
            meaning = row.findViewById(R.id.char_row_meaning);
            delete = row.findViewById(R.id.char_row_delete);

            row.setOnClickListener(x -> {
                Intent intent = new Intent(row.getContext(), UpdateCard.class);
                intent.putExtra("card", card);
                intent.putExtra("rowNumber", getAdapterPosition());
                ((Activity) context).startActivityForResult(intent, EDIT_RESULT);
            });

            delete.setOnClickListener(x -> {
                Database.getInstance(row.getContext()).cards().remove(card);
                Row.this.removeCard(getAdapterPosition());
            });

        }

    }

}
