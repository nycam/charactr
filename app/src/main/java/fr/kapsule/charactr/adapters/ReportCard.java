package fr.kapsule.charactr.adapters;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;


import fr.kapsule.charactr.R;
import fr.kapsule.charactr.activities.Main;

public class ReportCard extends RecyclerView.Adapter<ReportCard.ReportCardView> {



    @NonNull
    @Override
    public ReportCardView onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View card = inflater.inflate(R.layout.test_summary, viewGroup, false);

        return new ReportCardView(card);
    }

    @Override
    public void onBindViewHolder(@NonNull ReportCardView reportCardView, int i) {
        reportCardView.nbGoodAnswer.setText(Integer.toString(Main.nbOfGoodAnswer()));
        reportCardView.nbTotalAnswer.setText(Integer.toString(Main.ACTUAL_NUMBER_OF_CARDS));
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int j = 0; j < 100; j++) {
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    reportCardView.progressbarHandler.sendEmptyMessage(0);
                }

            }
        }).start();
    }


    @Override
    public int getItemCount() {
        return 1;
    }

    class ReportCardView extends RecyclerView.ViewHolder{
        private TextView percent;
        public TextView nbGoodAnswer;
        public TextView nbTotalAnswer;
        public ProgressBar bar;
        public Handler progressbarHandler = new Handler(){
            int progress = 0;
            @Override
            public void handleMessage(Message msg) {
                if(progress<(Main.nbOfGoodAnswer()*100)/Main.ACTUAL_NUMBER_OF_CARDS){
                    progress++;
                    bar.setProgress(progress);
                    percent.setText(""+progress+" %");
                }
            }
        };
        public ReportCardView(View card){
            super(card);
            this.nbGoodAnswer = card.findViewById(R.id.tv_nbGood);
            this.nbTotalAnswer = card.findViewById(R.id.tv_nbTotal);
            this.bar = card.findViewById(R.id.progressBar);
            this.percent = card.findViewById(R.id.tv_percent);
        }
    }
}
