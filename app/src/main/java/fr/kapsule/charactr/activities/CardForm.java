package fr.kapsule.charactr.activities;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import fr.kapsule.charactr.R;

public class CardForm {

    private Button validate;
    private EditText txtSymbole;
    private EditText txtMeaning;

    public CardForm(AppCompatActivity v) {
        this.validate = v.findViewById(R.id.pb_add_card);
        this.txtSymbole = v.findViewById(R.id.add_card_symbole);
        this.txtMeaning = v.findViewById(R.id.add_card_meaning);
    }

    public CardForm setAction(SendTxt callback) {
        this.validate.setOnClickListener(x -> callback.execute(txtSymbole.getText().toString(), txtMeaning.getText().toString()));
        return this;
    }

    /**
     * Change juste le texte "ok" en "mettre à jour"
     */
    public CardForm setUpdateButton() {
        validate.setText("Mettre à jour");
        return this;
    }

    public interface SendTxt {
        void execute(String meaning, String Symbol);
    }

    public CardForm setTxtSymbole(String txtSymbole) {
        this.txtSymbole.setText(txtSymbole);
        return this;
    }

    public CardForm setTxtMeaning(String txtMeaning) {
        this.txtMeaning.getText().append(txtMeaning);
        return this;
    }

}
