package fr.kapsule.charactr.activities;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.widget.Button;
import android.widget.Toast;

import com.yuyakaido.android.cardstackview.CardStackLayoutManager;
import com.yuyakaido.android.cardstackview.CardStackListener;
import com.yuyakaido.android.cardstackview.CardStackView;
import com.yuyakaido.android.cardstackview.Direction;
import com.yuyakaido.android.cardstackview.StackFrom;
import com.yuyakaido.android.cardstackview.SwipeAnimationSetting;

import java.util.Collections;
import java.util.List;

import fr.kapsule.charactr.R;
import fr.kapsule.charactr.adapters.ReportCard;
import fr.kapsule.charactr.adapters.TinderCard;
import fr.kapsule.charactr.models.Card;
import fr.kapsule.charactr.models.Database;

public class Main extends AppCompatActivity implements CardStackListener {

    /**
     * Codes to execute code at the end of an activity
     */
    public static final int ADD_CODE = 1;
    public static final int MANAGE_CODE = 2;


    public static final int MAX_NUMBER_OF_CARDS = 10;

    /**
     * Dirty, needs to be changed
     */
    public static int ACTUAL_NUMBER_OF_CARDS = 0;

    public final int MINIMUM_AMOUNT_OF_CARDS = 3;
    private boolean bilanPhase = false;
    private static int remainingCard = 0;
    private static int numberOfKnownCard = 0;
    private static boolean finishedList = false;
    private RecyclerView.Adapter cardAdapter;

    private Button pb_je_sais;
    private Button pb_je_ne_sais_pas;
    private CardStackView stackView;
    private CardStackLayoutManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pb_je_sais = findViewById(R.id.pb_i_know);
        pb_je_ne_sais_pas = findViewById(R.id.pb_i_dont_know);

        initCard();
        initEvents();
    }

    private void initEvents() {

        SwipeAnimationSetting.Builder settingBuilder = new SwipeAnimationSetting.Builder()
                                                        .setDuration(600)
                                                        .setInterpolator(new AccelerateInterpolator());

        pb_je_ne_sais_pas.setOnClickListener( x -> {
            SwipeAnimationSetting setting = settingBuilder.setDirection(Direction.Left).build();
            manager.setSwipeAnimationSetting(setting);
            stackView.swipe();
        });

        pb_je_sais.setOnClickListener(x -> {
            SwipeAnimationSetting setting = settingBuilder.setDirection(Direction.Right).build();
            manager.setSwipeAnimationSetting(setting);
            stackView.swipe();
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.nav_add_card:
                startActivityForResult(new Intent(this, AddCard.class), ADD_CODE);
                break;

            case R.id.nav_manage_cards:
                startActivityForResult(new Intent(this, SymbolList.class), MANAGE_CODE);
                break;
            case R.id.nav_refresh_stack:
                this.initCard();
                break;
            default:
                System.out.println("Not implemented");

        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        System.out.println(requestCode);
        if(requestCode == ADD_CODE) {
            System.out.println(resultCode);
            if(resultCode == RESULT_OK) {
                initCard();
            }
        }
    }

    private void initCard(){
        manager = new CardStackLayoutManager(this, this);
        stackView = findViewById(R.id.card_view);

        manager.setStackFrom(StackFrom.Top);
        manager.setVisibleCount(3);
        manager.setDirections(Direction.HORIZONTAL);

        stackView.setLayoutManager(manager);
        List<Card> cards = Database.getInstance(this).cards().getAllCards();
        if (cards.size() >= MINIMUM_AMOUNT_OF_CARDS) {
            Collections.shuffle(cards);
            List<Card> sessionCards = cards.subList(0, cards.size() >= MAX_NUMBER_OF_CARDS ? MAX_NUMBER_OF_CARDS :cards.size());
            System.out.println(cards.size());
            remainingCard = sessionCards.size();
            ACTUAL_NUMBER_OF_CARDS = remainingCard;
            cardAdapter = new TinderCard(sessionCards);
            stackView.setAdapter(cardAdapter);
            numberOfKnownCard = 0;
            finishedList = false;
        } else {
            Toast.makeText(this, "VOUS AVEZ BESOIN D'AU MOINS "+MINIMUM_AMOUNT_OF_CARDS+" CARTES", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onCardDragging(Direction direction, float ratio) {



    }

    @Override
    public void onCardSwiped(Direction direction) {
        if(this.bilanPhase){
            this.initCard();
            this.bilanPhase = false;
        }else{
            this.numberOfKnownCard = (direction == Direction.Left) ? numberOfKnownCard : numberOfKnownCard+1;
            this.remainingCard--;
            finishedList = (remainingCard == 0) ? true: false;
            if(finishedList){
                this.showBilan();
                this.bilanPhase =true;
            }
        }
    }

    @Override
    public void onCardRewound() {

    }

    @Override
    public void onCardCanceled() {

    }

    @Override
    public void onCardAppeared(View view, int position) {

    }

    @Override
    public void onCardDisappeared(View view, int position) {

    }
    public static boolean isListFinished(){
        return finishedList;
    }
    public static int nbOfGoodAnswer() {
        return numberOfKnownCard;
    }

    private void showBilan(){
        CardStackLayoutManager manager = new CardStackLayoutManager(this, this);
        manager.setStackFrom(StackFrom.Top);
        manager.setVisibleCount(1);
        manager.setDirections(Direction.HORIZONTAL);
        cardAdapter = new ReportCard();
        CardStackView stackView = findViewById(R.id.card_view);
        stackView.setLayoutManager(manager);
        stackView.setAdapter(cardAdapter);
    }
}
