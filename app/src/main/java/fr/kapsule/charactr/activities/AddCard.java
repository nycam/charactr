package fr.kapsule.charactr.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import fr.kapsule.charactr.R;
import fr.kapsule.charactr.models.Card;
import fr.kapsule.charactr.models.Database;

public class AddCard extends AppCompatActivity {




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_card);

        CardForm cardForm = new CardForm(this);

        cardForm.setAction((symbol, meaning) -> {
            Database.getInstance(this)
                    .cards()
                    .insert(new Card(symbol, meaning, ""));
            setResult(RESULT_OK);
            finish();
        });

    }

}
