package fr.kapsule.charactr.activities;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Toast;

import fr.kapsule.charactr.R;
import fr.kapsule.charactr.adapters.Row;
import fr.kapsule.charactr.models.Card;
import fr.kapsule.charactr.models.Database;

public class SymbolList extends AppCompatActivity {

    private Row adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_symbol_list);

        adapter = new Row(Database.getInstance(this).cards().getAllCards());
        RecyclerView rView = findViewById(R.id.recycle_view_symbol);
        rView.setAdapter(adapter);
        rView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == Row.RowView.EDIT_RESULT) {
            if(resultCode == RESULT_OK) {
                int row_number = data.getIntExtra("rowNumber", 0);
                Card card = (Card)data.getSerializableExtra("card");
                adapter.notifyItemChanged(row_number);
                adapter.updateCard(row_number, card);
                Database.getInstance(this).cards().update(card);
                Toast.makeText(this, "Carte mise à jour", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
