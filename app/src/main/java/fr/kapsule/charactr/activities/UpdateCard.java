package fr.kapsule.charactr.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import fr.kapsule.charactr.R;
import fr.kapsule.charactr.models.Card;
import fr.kapsule.charactr.models.Database;

public class UpdateCard extends AppCompatActivity {

    private static Card card;
    private int row_number = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_card);

        getDataFromIntent();

        CardForm cardForm = new CardForm(this);
        cardForm.setUpdateButton()
                .setTxtMeaning(card.getSignification())
                .setTxtSymbole(card.getSymbol())
                .setAction((symbol, meaning) -> {
                    card.setSignification(meaning)
                            .setSymbol(symbol);
                    Database.getInstance(this).cards().update(card);
                    Intent intent = new Intent();
                    intent.putExtra("rowNumber", row_number);
                    intent.putExtra("card", card);
                    setResult(RESULT_OK, intent);
                    finish();
                });
    }

    private void getDataFromIntent() {
        card = (Card) getIntent().getSerializableExtra("card");
        row_number = getIntent().getIntExtra("rowNumber", 0);
    }
}
